# 666-1980

`3 books` `all files encrypted`

---

[Methods of Mathematical Economics](./bok%253A978-3-662-25317-5.zip)
<br>
Linear and Nonlinear Programming, Fixed-Point Theorems
<br>
Joel Franklin in Undergraduate Texts in Mathematics (1980)

---

[Book Elementary Stability and Bifurcation Theory](./bok%253A978-1-4684-9336-8.zip)
<br>
Elementary Stability and Bifurcation Theory
<br>
Gérard Iooss, Daniel D. Joseph in Undergraduate Texts in Mathematics (1980)

---

[Book Elementary Analysis: The Theory of Calculus](./bok%253A978-1-4757-3971-8.zip)
<br>
Elementary Analysis: The Theory of Calculus
<br>
Kenneth A. Ross in Undergraduate Texts in Mathematics (1980)

---
